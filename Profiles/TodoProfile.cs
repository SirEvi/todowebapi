using AutoMapper;
using TodoWebAPI.Dtos;
using TodoWebAPI.Models;

namespace TodoWebAPI.Profiles
{
    public class TodoProfile : Profile
    {
        public TodoProfile()
        {
            // From Source to target
            CreateMap<Todo, TodoReadDto>();
            CreateMap<TodoCreateDto, Todo>();
            CreateMap<TodoUpdateDto, Todo>();
            CreateMap<Todo, TodoUpdateDto>();
        }
    }
}