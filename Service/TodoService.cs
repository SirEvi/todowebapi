using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TodoWebAPI.Data;
using TodoWebAPI.Dtos;
using TodoWebAPI.Interface;
using TodoWebAPI.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace TodoWebAPI.Service
{
    public class TodoService : ITodo
    {
        private readonly TodoContext _context;
        private IMapper _mapper;
        private ILoggerManager _logger;

        public TodoService(TodoContext contect, IMapper mapper, ILoggerManager logger)
        {
            _context = contect;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<TodoReadDto> CreateTodo(TodoCreateDto newTodo)
        {
            var todoModel = _mapper.Map<Todo>(newTodo);
            _context.Todos.Add(todoModel);
            await _context.SaveChangesAsync();

            var todoReadDto = _mapper.Map<TodoReadDto>(todoModel);
            _logger.LogInfo($"Todo task with Id = {todoModel.Id} created");
            return todoReadDto;
        }

        public async Task<TodoReadDto> DeleteTodo(int id)
        {
            var todoFromDb = _context.Todos.FirstOrDefault(t => t.Id == id);
            if (todoFromDb == null) 
            {
                _logger.LogError($"Todo task with Id = {id} not found");
            }
            _context.Todos.Remove(todoFromDb);
            await _context.SaveChangesAsync();

            var todoReadDto = _mapper.Map<TodoReadDto>(todoFromDb);
            _logger.LogError($"Todo task with Id = {id} has been deleted from the database!");
            return todoReadDto;
        }

        public async Task<IEnumerable<TodoReadDto>> GetAllTodos()
        {
            var todosFromDb =  _context.Todos.ToList();
            var todoReadDto = _mapper.Map<IEnumerable<TodoReadDto>>(todosFromDb);
            _logger.LogInfo("Todo apllication displayed");
            return todoReadDto;
        }

        public async Task<TodoReadDto> GetTodoById(int id)
        {
            var todoFromDb = _context.Todos.FirstOrDefault(t => t.Id == id);           
            var todoReadDto =  _mapper.Map<TodoReadDto>(todoFromDb); 
            return todoReadDto;
        }

        public async Task<TodoReadDto> UpdateTodo(TodoUpdateDto updatetodo)
        {
            var todoFromDb =  _context.Todos.FirstOrDefault(t => t.Id == updatetodo.Id);
            if (todoFromDb == null) 
            {
                _logger.LogError("Todo you are about to update cannot be located, kindly check your Id");
            }
            todoFromDb.Task = updatetodo.Task;
            todoFromDb.EstimatedTime = updatetodo.EstimatedTime;
            todoFromDb.Individual = updatetodo.Individual;
            todoFromDb.State = updatetodo.State;
            

            var todoReadDto =  _mapper.Map<TodoReadDto>(todoFromDb);
            _logger.LogInfo($"Todo task with Id:{updatetodo.Id} has just been updated. ");
            return todoReadDto;

        }
    }
}