using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using TodoWebAPI.Dtos;
using TodoWebAPI.Interface;
using TodoWebAPI.Models;
using TodoWebAPI.Service;

namespace TodoWebAPI.Controllers
{
    [Route("api/todos")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        private readonly ITodo _repository;
        private IMapper _mapper;
        private ILoggerManager _loggerManager;

        public TodoController(ITodo repository, IMapper mapper, ILoggerManager loggerManager)
        {
            _repository = repository;
            _mapper = mapper;
            _loggerManager = loggerManager;
        }

        //Get api/todos
        [HttpGet]
        public async Task<IActionResult> GetAllTodos()
        {
            var todoList = await _repository.GetAllTodos();
            return Ok(todoList);
        }

        //Get api/todos/(id)
        [HttpGet("{id}", Name="GetTodoById")]
        public async Task<IActionResult> GetTodoById(int id)
        {
            return Ok(await _repository.GetTodoById(id));
        }

        //Post api/todos
        [HttpPost]
        public async Task<IActionResult> CreateTodo(TodoCreateDto todoCreateDto)
        {
            var todoReadDto = await _repository.CreateTodo(todoCreateDto);          
            return CreatedAtRoute(nameof(GetTodoById), new {Id = todoReadDto.Id}, todoReadDto);
        }
        
        //Put api/todos
        [HttpPut]
        public async Task<IActionResult> UpdateTodo(TodoUpdateDto todoUpdateDto)
        {
            return Ok(await _repository.UpdateTodo(todoUpdateDto));
        }

        //Delete api/todos{id}
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodo(int id)
        {
            return Ok(await _repository.DeleteTodo(id));
        }

        //Patch api/todo
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallUpdate(int id, JsonPatchDocument<TodoUpdateDto> patchDto)
        {
            var todoModelFromDb = _repository.GetTodoById(id);
            if (todoModelFromDb == null)
            {
                _loggerManager.LogError($"Todo task with Id = {id} not found");
                return NotFound();
            }
            var todoToPatch = _mapper.Map<TodoUpdateDto>(todoModelFromDb);
            patchDto.ApplyTo(todoToPatch, ModelState);
            if (!TryValidateModel(todoToPatch))
            {
                return ValidationProblem(ModelState);
            }
            await _mapper.Map(todoToPatch, todoModelFromDb);
            return NoContent();
        }


    }
}