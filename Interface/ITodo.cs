using Microsoft.AspNetCore.JsonPatch;
using System.Collections.Generic;
using System.Threading.Tasks;
using TodoWebAPI.Dtos;
using TodoWebAPI.Models;

namespace TodoWebAPI.Interface
{
    public interface ITodo
    {
        Task<IEnumerable<TodoReadDto>> GetAllTodos();
        Task<TodoReadDto> GetTodoById(int id);
        Task<TodoReadDto> CreateTodo(TodoCreateDto newTodo);
        Task<TodoReadDto> UpdateTodo(TodoUpdateDto updatetodo);
        Task<TodoReadDto> DeleteTodo(int id);

    }
}