using System.ComponentModel;

namespace TodoWebAPI.Models
{
    public enum TodoState
    {
        [Description("Todo task is yet to start")]
        Inprogress,

        [Description("Todo task has started")]
        Started,

        [Description("Todo task is on hold")]
        OnHold,

        [Description("Todo task completed")]
        Ended,

    }
}