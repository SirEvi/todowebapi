using System.ComponentModel.DataAnnotations;
using TodoWebAPI.Models;

namespace TodoWebAPI.Dtos
{
    public class TodoCreateDto
    {
        [Required]
        [MaxLength(250)]
        public string Task { get; set; }

        [Required]
        [MaxLength(100)]
        public string EstimatedTime { get; set; }

        [Required]
        [MaxLength(50)]
        public string Individual { get; set; }

        [Required]      
        public TodoState State { get; set; } = TodoState.Inprogress;
    }
}