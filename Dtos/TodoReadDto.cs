using TodoWebAPI.Models;

namespace TodoWebAPI.Dtos
{
    public class TodoReadDto
    {
        public int Id { get; set; }
        public string Task { get; set; }
        public string EstimatedTime { get; set; }
        public string Individual { get; set; }     
        public TodoState State { get; set; } = TodoState.Inprogress;
    }
}